package com.expense.manager.utils;

import android.content.Context;

import com.expense.manager.databases.ExpenseDatabaseHelper;

import java.io.File;

import androidx.annotation.NonNull;

public class CacheUtils {

    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();

            //Delete caches
            deleteDir(dir);

            //Delete database
            deleteDatabase(context, ExpenseDatabaseHelper.DATABASE_NAME);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (String aChildren : children) {
                boolean success = deleteDir(new File(dir, aChildren));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    private static void deleteDatabase(Context context, @NonNull String databaseName) {

        context.deleteDatabase(databaseName);
    }
}
