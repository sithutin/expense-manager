package com.expense.manager;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Setting up the activity for full screen mode.
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

        TextView splashText = findViewById(R.id.splash_text);
        Typeface quicksandBold = Typeface.createFromAsset(getAssets(), "fonts/Quicksand_Bold.ttf");
        splashText.setTypeface(quicksandBold);

        new Handler().postDelayed(() -> {
            startActivity(new Intent(getApplicationContext(), LandingActivity.class));
            finish();
        }, 1500);
    }
}
