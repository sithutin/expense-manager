package com.expense.manager.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.expense.manager.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class CategorySpinnerAdapter extends ArrayAdapter<String> {

    private List<String> categoryList;
    private Typeface quicksand_medium;

    public CategorySpinnerAdapter(Context context, List<String> categoryList) {

        super(context, 0, categoryList);
        this.categoryList = categoryList;
        quicksand_medium =
                Typeface.createFromAsset(context.getAssets(), "fonts/Quicksand_Medium.ttf");
    }

    @NonNull
    @Override
    public View getView(int position,
            @Nullable View convertView,
            @NonNull ViewGroup parent) {

        String category = categoryList.get(position);

        View view = convertView;
        if (view == null) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.spinner_item, parent, false);
        }

        TextView spinnerText = view.findViewById(R.id.spinner_text_category);
        spinnerText.setTypeface(quicksand_medium);
        spinnerText.setText(category);
        return view;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView,
            @NonNull ViewGroup parent) {
        TextView dropDownText = (TextView) super.getDropDownView(position, convertView, parent);
        dropDownText.setTypeface(quicksand_medium);
        return dropDownText;
    }
}
