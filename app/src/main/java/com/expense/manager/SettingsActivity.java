package com.expense.manager;

import android.content.Intent;
import android.os.Bundle;
import android.widget.RelativeLayout;

import com.expense.manager.utils.CacheUtils;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class SettingsActivity extends AppCompatActivity {

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

          /*
        Setting up the toolbar for the activity.
         */
        toolbar = findViewById(R.id.toolbar_transactions);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Settings");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        RelativeLayout rlDeleteAppData = findViewById(R.id.rl_delete_app_data);
        rlDeleteAppData.setOnClickListener(v -> new AlertDialog.Builder(this)
                .setTitle("Clear all data!")
                .setMessage("Are you sure you want to delete?")
                .setPositiveButton("Delete", (dialog, which) -> {
                    CacheUtils.deleteCache(this);
                    this.resetApp();
                })
                .setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss()).show());
    }

    private void resetApp() {
        Intent i = getBaseContext()
                .getPackageManager()
                .getLaunchIntentForPackage(getBaseContext().getPackageName());
        assert i != null;
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        finish();
    }
}
